package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;


@SpringBootApplication
@EnableEurekaServer
public class SpringEurekaEvFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEurekaEvFinalApplication.class, args);
	}

}
