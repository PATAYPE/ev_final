
$(document).ready(function () {
    $("#btn-buscar").click(function () {
		
		var nomMedicamento = $("#txtMedicamento").val();
		
//        alert("BUSCAR");
        $.ajax({
            type: "GET",
            url: "http://localhost:8099/micro-evfinal/farmacia/listarFarmacia/"+nomMedicamento,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
				 for(var i= 0; i< data.length ; i++){
					 var idfarmacia = data[i].id_farmacia;
					 var precio = data[i].precio;
					 var imagen = data[i].medicamento.imagen;

                     $("#pad_medicamentos").append("<div class=\"col-xs-3\"><div class=\"radio\"><img width=\"150px\" height=\"150px\" class=\"img-responsive\" src="+imagen+" alt=\"Paracetamol\"><label><input type=\"radio\" name=\"optradiox\" value="+idfarmacia+">"+precio+"</label></div></div>");
				 }
            },

            error: function (XMLHttpRequest, textStatus, errorThrown) {
                toastr.error("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
            }
        });
    });

    

    $("#btn-registrar").click(function () {
        alert("REGISTRAR");
		
        // validar que haya al menos un radio

        if( $('input[type=radio]').is(':checked') ){
            
        var idfarmacia = $('input[name="optradiox"]:checked').val();	
        //debe generar una estrucutra json, puede guiarse de SpringBootNota del método POST callAjax del js
        //en este caso no es necesario los token csrf;
        //var json_input = .... 
        
        var input = {
                "farmacia":{
                    "id_farmacia" : idfarmacia
                }
        };

        console.log(input);

        //Ejemplo en español de peticions ajax con jquery método post
        //https://es.stackoverflow.com/questions/24583/enviar-post-a-php-por-medio-de-ajax
        
        //Para crear el json_input recuerde que JSON es solo un string|cadena de texto, que tiene un formato especial
        //Documentacion JSON https://www.w3schools.com/js/js_json.asp
        
        //Recurde que siempre puede probar sus servicios con POSTMAN
        
            $.ajax({
                type: "POST",
                url: "http://localhost:8099/micro-evfinal/compra/registrarcompra",
                contentType: "application/json; charset=utf-8",
                data : JSON.stringify(input),	
                success: function (data) {
                    //SI EL SERVICIO RETORNA EXITO EVALUAR
                    if (data == '1') {
                        toastr.info("Se registró");
                    } else {
                        toastr.warning("Error al registrar " + data);
                    }
                },

                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    toastr.error("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
                }
            });

        }
        else{
            alert("Debe seleccionar al menos un producto ");
        }

    });

   
});