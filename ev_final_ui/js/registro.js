
$(document).ready(function () {
    //Se ejecutará apenas cargue la página
    alert("CARGUE");
	
    $.ajax({
        type: "GET",
        url: "http://localhost:8099/micro-evfinal/compra/listarcompra",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //HACER ALGO CON LA DATA
            //IMPRIMIR EN ul-li o como guste

            console.log(data);
            for(var i =0; i< data.length; i++){
                var medicamento = data[i].farmacia.medicamento.nombre;
                var farmacia = data[i].farmacia.nombre;
                $("#lista_compra").append("<li class=\"list-group-item d-flex justify-content-between align-items-center\">"+medicamento+"<span class=\"badge badge-primary badge-pill\">"+farmacia+"</span></li>");
            }
        },

        error: function (XMLHttpRequest, textStatus, errorThrown) {
            toastr.error("Request: " + XMLHttpRequest.toString() + "\n\nStatus: " + textStatus + "\n\nError: " + errorThrown);
        }
    });

});