package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mitocode.model.Medicamento;

public interface IMedicamentoDao extends JpaRepository<Medicamento, Integer> {

}
