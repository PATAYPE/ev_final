package com.mitocode.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Farmacia;

public interface IFarmaciaDao extends JpaRepository<Farmacia, Integer> {

	// @Query("SELECT m FROM Movie m WHERE m.title LIKE %:title%")
	@Query(value = "select f.id_farmacia, f.nombre_farmacia, m.id_medicamento, m.nombre_medicamento, m.url_imagen, f.precio  from medicamento m\r\n"
			+ "inner join farmacia f\r\n"
			+ "on m.id_medicamento = f.id_medicamento \r\n"
			+ "where UPPER(m.nombre_medicamento) LIKE  %:title%", nativeQuery = true)
	public List<Farmacia> listarFarmaciabyName(@Param("title") String title);
}
