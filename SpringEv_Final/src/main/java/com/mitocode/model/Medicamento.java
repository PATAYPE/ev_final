package com.mitocode.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "medicamento" )
public class Medicamento {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private int id_medicamento;
	
	@Column(name = "nombre_medicamento")
	private String nombre;
	
	@Column(name = "url_imagen")
	private String imagen;
	

	public int getId() {
		return id_medicamento;
	}

	public void setId(int id) {
		this.id_medicamento = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	
	
}
