package com.mitocode.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name="farmacia")
public class Farmacia {


	@Id
	@GeneratedValue ( strategy = GenerationType.IDENTITY )
	private int id_farmacia;
	
	@Column( name="nombre_farmacia")
	private String nombre;
	
	@Column(name= "precio")
	private double precio;
	
	@ManyToOne
	@JoinColumn(name = "id_medicamento", nullable = false)
	private Medicamento medicamento;

	
	//@OneToMany(mappedBy = "farmacia" , cascade = { CascadeType.ALL }, fetch = FetchType.LAZY, orphanRemoval = true)
	//private List<Compra> compra; 
	
	

	public int getId_farmacia() {
		return id_farmacia;
	}

	public void setId_farmacia(int id_farmacia) {
		this.id_farmacia = id_farmacia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public double getPrecio() {
		return precio;
	}


	public void setPrecio(double precio) {
		this.precio = precio;
	}


	public Medicamento getMedicamento() {
		return medicamento;
	}


	public void setMedicamento(Medicamento medicamento) {
		this.medicamento = medicamento;
	}


	/*public List<Compra> getCompra() {
		return compra;
	}


	public void setCompra(List<Compra> compra) {
		this.compra = compra;
	}*/
	
}
