package com.mitocode.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "compra")
public class Compra {

	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private int id_compra;
	
	@ManyToOne
	@JoinColumn( name="id_farmacia", nullable = false)
	private Farmacia farmacia;

	public int getId() {
		return id_compra;
	}

	public void setId(int id) {
		this.id_compra = id;
	}

	public Farmacia getFarmacia() {
		return farmacia;
	}

	public void setFarmacia(Farmacia farmacia) {
		this.farmacia = farmacia;
	}
	
	
}
