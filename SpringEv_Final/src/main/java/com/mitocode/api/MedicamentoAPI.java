package com.mitocode.api;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.MediaTray;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Medicamento;
import com.mitocode.service.IMedicamentoService;

@RestController
@RequestMapping("/medicamento")
public class MedicamentoAPI {

	@Autowired
	private IMedicamentoService service;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping(value ="/listarMedicamento/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Medicamento>> listarMedicamentos(){
		logger.info("Entro getAll [Medicamento]");
		
		List<Medicamento> lista = new ArrayList<Medicamento>();
		try {
			
			lista = service.findAll();
			
		} catch (Exception e) {
			logger.error("Error ->", e);
			return new ResponseEntity<List<Medicamento>>(lista, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Entro getAll [Medicamento]");
		return new ResponseEntity<List<Medicamento>>(lista, HttpStatus.OK);
	}
	
}
