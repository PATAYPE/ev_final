package com.mitocode.api;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Compra;
import com.mitocode.service.ICompraService;

@RestController
@RequestMapping("/compra")
public class CompraAPI {

	@Autowired
	private ICompraService service;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	

	@GetMapping(value="/listarcompra", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Compra>> listarCompra(){
		logger.info("Entro getAll [listarCompra]");
		
		List<Compra> lista = new ArrayList<Compra>();
		try {
			lista = service.findAll();	
		} catch (Exception e) {
			logger.error("Error ->", e);
			return new ResponseEntity<List<Compra>>(lista, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Entro getAll [listarCompra]");
		return new ResponseEntity<List<Compra>>(lista, HttpStatus.OK);
	}

	@PostMapping(value = "/registrarcompra" , consumes = MediaType.APPLICATION_JSON_VALUE, produces =  MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> registrarCompra(@RequestBody Compra compra ){
		logger.info("Entro save [registroCompra]");
		Compra createdCompra = null;
		int rpta  = 0;
		try {
			
			createdCompra = service.create(compra);
			
			if(createdCompra!=null) rpta = 1;
			else rpta = 0;
			
		} catch (Exception e) {
			logger.error("Error ->", e);
			return new ResponseEntity<Integer>(rpta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Entro save [registroCompra]");
		return new ResponseEntity<Integer>(rpta, HttpStatus.OK);
	}
}
