package com.mitocode.api;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Farmacia;
import com.mitocode.service.IFarmaciaService;

@RestController
@RequestMapping("/farmacia")
public class FarmaciaAPI {

	@Autowired
	private IFarmaciaService service;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@GetMapping(value = "/listarFarmacia/{nombre}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Farmacia>> listarFarmacias(@PathVariable("nombre") String nombre){
		logger.info("Entro getAll [Farmacia]");
		List<Farmacia> lista = new ArrayList<>();
		
		try {
			lista = service.listarFarmaciabyName(nombre);
			
		} catch (Exception e) {
			logger.error("Error ->", e);
			return new ResponseEntity<List<Farmacia>>(lista, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Salio getAll [Farmacia]");
		return new ResponseEntity<List<Farmacia>>(lista, HttpStatus.OK);
	}
	
}
