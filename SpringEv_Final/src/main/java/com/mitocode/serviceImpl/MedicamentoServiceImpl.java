package com.mitocode.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.IMedicamentoDao;
import com.mitocode.model.Medicamento;
import com.mitocode.service.IMedicamentoService;

@Service
public class MedicamentoServiceImpl implements IMedicamentoService{

	@Autowired
	private IMedicamentoDao dao;
	
	
	@Override
	public List<Medicamento> findAll() {		
		return dao.findAll();
	}

	@Override
	public Medicamento create(Medicamento obj) {
		
		return dao.save(obj);
	}

	@Override
	public Medicamento find(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public Medicamento update(Medicamento obj) {
		return dao.save(obj);
	}

	@Override
	public void delete(Integer id) {
		dao.delete(id);
		
	}

}
