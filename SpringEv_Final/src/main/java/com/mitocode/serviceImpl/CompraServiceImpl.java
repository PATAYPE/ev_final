package com.mitocode.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ICompraDao;
import com.mitocode.model.Compra;
import com.mitocode.service.ICompraService;

@Service
public class CompraServiceImpl implements ICompraService{
	
	@Autowired
	private ICompraDao dao;

	
	@Override
	public List<Compra> findAll() {
		return dao.findAll();
	}

	@Override
	public Compra create(Compra obj) {
		return dao.save(obj);
	}

	@Override
	public Compra find(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public Compra update(Compra obj) {		
		return dao.save(obj);
	}

	@Override
	public void delete(Integer id) {
		dao.delete(id);
		
	}

}
