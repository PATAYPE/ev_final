package com.mitocode.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ICompraDao;
import com.mitocode.dao.IFarmaciaDao;
import com.mitocode.model.Farmacia;
import com.mitocode.service.IFarmaciaService;

@Service
public class FarmaciaServiceImpl implements IFarmaciaService{

	@Autowired
	private IFarmaciaDao dao;

	
	@Override
	public List<Farmacia> findAll() {
		
		return dao.findAll();
	}

	@Override
	public Farmacia create(Farmacia obj) {
		return dao.save(obj);
	}

	@Override
	public Farmacia find(Integer id) {
		return dao.findOne(id);
	}

	@Override
	public Farmacia update(Farmacia obj) {
		
		return dao.save(obj);
	}

	@Override
	public void delete(Integer id) {
		dao.delete(id);
	}

	@Override
	public List<Farmacia> listarFarmaciabyName(String nombre) {
		return dao.listarFarmaciabyName(nombre);
	}

}
