package com.mitocode.service;

import java.util.List;


import com.mitocode.model.Farmacia;

public interface IFarmaciaService extends ICrud<Farmacia>{

	
	public List<Farmacia> listarFarmaciabyName(String nombre);
	
}
